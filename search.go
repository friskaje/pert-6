package main

import "fmt"

func main() {
	var data = map[string]mahasiswa{
		"16115966": {
			"Ariel Noah",
			"2ka15",
		},
		"10119651": {
			"Andhika Kangen Band",
			"4kA01",
		},
		"12119523": {
			"Friska Putri Rahma Sukandar",
			"2KA08",
		},
	}
	var search string
	fmt.Print("Masukkan NPM anda? ")
	fmt.Scanf("%s", &search)
	var value, ok = data[search]

	if ok {
		fmt.Printf("Nama anda %s \nKelas anda %s", value.Nama, value.kelas)
	} else {
		fmt.Println("Data tidak ditemukan")
	}
}

type mahasiswa struct {
	Nama  string
	kelas string
}
